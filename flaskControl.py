#coding:utf-8
import ctypes
import sys
import os

so = ctypes.CDLL("./flaskLed.so")
so.init()

def gpioOutput(number, flags):
    number = int(number)
    flags = int(flags)
    if flags == 0:
        so.control(number, int(0))
    elif flags == 1:
        so.control(number, int(1))
