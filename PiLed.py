from flask import Flask, jsonify, render_template, request
import flaskControl

app = Flask(__name__)

@app.route('/add')
def add_numbers():
    gpiovalue = request.args.get('a', 0)
    gpionumber = gpiovalue.split('#')[0]
    gpipstate = gpiovalue.split('#')[1]
    if gpipstate == 'k':
        flaskControl.gpioOutput(gpionumber, 1)
    if gpipstate == 'g':
        flaskControl.gpioOutput(gpionumber, 0)
    return gpiovalue

@app.route('/')
def index():
    return render_template('index.html')


if __name__ == '__main__':
    app.run(
		host="0.0.0.0",
		port=int("5000"),
		debug=True
	)
