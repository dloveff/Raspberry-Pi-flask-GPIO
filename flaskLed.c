#include <wiringPi.h>
#include <stdio.h>

void init()
{
	wiringPiSetup();
}

//0ΪLOW�� ��0ΪHIGH
void control(int pin, int flag)
{
	pinMode(pin, OUTPUT);
	digitalWrite(pin, flag);
}
